<?php


namespace App\ExternalApis;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * Class HuntFlowApi
 * @package App\ExternalApis
 */
class HuntFlowApi
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var int
     */
    private $accountId;

    /**
     * HuntFlowApi constructor.
     * @param int $accountId
     * @param Client $client
     */
    public function __construct(int $accountId, Client $client)
    {
        $this->client = $client;
        $this->accountId = $accountId;
    }

    /**
     * @param int $page
     * @param bool $opened
     * @return array
     * @throws GuzzleException
     */
    public function getVacanciesList(int $page = 1, bool $opened = true): array
    {
        $query = [
            'page' => $page
        ];

        if ($opened) {
            $query['opened'] = 1;
        }

        $data = [
            'query' => $query
        ];

        $response = $this->sendRequest(
            'GET',
            '/account/' . $this->accountId . '/vacancies/',
            $data
        );


        return $this->decodeResponse($response);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @return ResponseInterface
     * @throws GuzzleException
     */
    private function sendRequest(string $method, string $url, array $data = []): ResponseInterface
    {
        return $this->client->request(
            $method,
            $url,
            $data
        );
    }

    /**
     * @param ResponseInterface|null $response
     * @return array
     */
    protected function decodeResponse(?ResponseInterface $response): array
    {
        if ($response === null) {
            return [];
        }

        $decoded = json_decode((string)$response->getBody(), true);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $decoded;
        }

        return [];
    }

    /**
     * @return array
     * @throws GuzzleException
     */
    public function getCompanyStructure(): array
    {
        $query = [

        ];

        $data = [
            'query' => $query
        ];

        $response = $this->sendRequest(
            'GET',
            '/account/' . $this->accountId . '/all_divisions',
            $data
        );


        return $this->decodeResponse($response);
    }

    /**
     * @param null $vacancyId
     * @return array
     * @throws GuzzleException
     */
    public function getManagers($vacancyId = null): array
    {
        $query = [];

        if ($vacancyId) {
            $query['vacancy'] = $vacancyId;
        }

        $data = [
            'query' => $query
        ];

        $response = $this->sendRequest(
            'GET',
            '/account/' . $this->accountId . '/members',
            $data
        );


        return $this->decodeResponse($response);
    }

    /**
     * @param int $id
     * @return array
     * @throws GuzzleException
     */
    public function getVacancyItem(int $id): array
    {
        $response = $this->sendRequest(
            'GET',
            '/account/' . $this->accountId . '/vacancies/' . $id
        );


        return $this->decodeResponse($response);
    }

    /**
     * @param string $resume
     * @return array
     * @throws GuzzleException
     */
    public function sendUploadedResume(string $resume): array
    {
        $response = $this->sendRequest(
            'POST',
            '/account/' . $this->accountId . '/upload',
            [
                'multipart' => [
                    [
                        'name' => 'file',
                        'contents'  => Utils::tryFopen($resume, 'r'),
                    ]
                ],
                'headers'  => ['X-File-Parse' => 'true']
            ]
        );

        return $this->decodeResponse($response);
    }

    /**
     * @param array $data
     * @return array
     * @throws GuzzleException
     */
    public function addCandidateToBase(array $data): array
    {
        $response = $this->sendRequest(
            'POST',
            '/account/' . $this->accountId . '/applicants',
            [RequestOptions::JSON => $data]
        );

        return $this->decodeResponse($response);
    }

    /**
     * @param int $vacancyId
     * @param int $candidateId
     * @param int|null $resumeId
     * @param string $comment
     * @return array
     * @throws GuzzleException
     */
    public function sendResponseToVacancy(
        int $vacancyId,
        int $candidateId,
        int $resumeId = null,
        string $comment = 'Добавлено с сайта rabota.cdek.ru'
    ): array {
        $data = [
            'vacancy' => $vacancyId,
            'status' => 1,
            'comment' => $comment,
            'files' => [
                ['id' =>  $resumeId]
            ]
        ];

        $response = $this->sendRequest(
            'POST',
            '/account/' . $this->accountId . '/applicants/' . $candidateId . '/vacancy',
            [RequestOptions::JSON => $data]
        );

        return $this->decodeResponse($response);
    }
}
